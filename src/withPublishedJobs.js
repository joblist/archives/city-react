import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeJobs from './serializers/jobs';

function buildRef(props) {
	// Load jobs from X days ago.
	const today = new Date()
	const priorDate = new Date().setDate(today.getDate() - 31)
	return firebase.firestore()
		.collection('jobs')
		.where('publishedAt', '>',  new Date(priorDate))
		.orderBy('publishedAt')
}

export default withFirebase(buildRef, serializeJobs);
