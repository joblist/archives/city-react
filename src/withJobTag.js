import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeJobs from './serializers/jobs';

function buildRef(props) {
	// Load jobs from X days ago.
	const today = new Date()
	const priorDate = new Date().setDate(today.getDate() - 32)
	return firebase.firestore()
		.collection('jobs')
		.where('publishedAt', '>',  new Date(priorDate))
		.where('tags', 'array-contains', props.match.params.tag)
		.orderBy('publishedAt')
}

export default withFirebase(buildRef, serializeJobs);
