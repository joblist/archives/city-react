import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeJobs from './serializers/jobs';


const buildRef = (props) => {
	const { user } = props;
	if (user) {
		return firebase.firestore()
			.collection('jobs')
			.where('user', '==', user.uid)
			.orderBy('createdAt')
	}
}

export default withFirebase(buildRef, serializeJobs);
