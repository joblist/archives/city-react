import React, {Component} from 'react';
import { Switch,
				 Route,
				 NavLink,
				 Redirect } from 'react-router-dom';
import withUser from './withUser';
import PrivateRoute from './components/PrivateRoute.js';
import PublicRoute from './components/PublicRoute.js';
import { isAuthenticated } from './actions/auth';
import NoMatchRoute from './components/NoMatchRoute';
import MapRoute from './components/MapRoute';
import JobsRoute from './components/JobsRoute';
import TagsJobs from './components/TagsJobs';
import AddJob from './components/AddJob';
import Info from './components/Info';
import MenuRoute from './components/MenuRoute';
import Login from './components/Login';
import Logout from './components/Logout';
import Register from './components/Register';
import Account from './components/Account';
import Logo from './components/Logo';

import NotificationDisplay from './components/NotificationDisplay';

class App extends Component {
	render() {
		const auth = isAuthenticated();

		return (
			<div className="App">
				<NotificationDisplay/>

				<aside className="App-aside">
					<nav className="Nav Nav--logo Nav--fromDesktop">
						<NavLink className="Nav-item Logo" to="/">
							<h1 title="Job List City">
								<Logo/>
							</h1>
						</NavLink>
					</nav>
					<nav className="Nav Nav--main Nav--fromMobile">
						<NavLink className="Nav-item" to="/jobs" title="List of jobs currently open.">Jobs</NavLink>
					</nav>
					<nav className="Nav Nav--main Nav--fromDesktop">
						{ auth && <NavLink className="Nav-item" to="/account" title="Your user account.">Account</NavLink> }
						{ !auth && <NavLink className="Nav-item" to="/login" title="Log into your user account.">Login</NavLink> }
						<NavLink className="Nav-item" to="/info" title="☮ What is this page about?">Info</NavLink>
					</nav>
					<nav className="Nav Nav--main Nav--menu">
						<NavLink className="Nav-item" to="/menu" title="Table of content, pages to navigate this site">
							⇶
						</NavLink>
					</nav>
				</aside>

				<main className="App-main">
					<Switch>
						<Redirect exact from='/' to='/jobs'/>

						<Route path="/menu" component={ MenuRoute }/>
						<Route path="/info" component={ Info }/>

						<Route path="/map" component={ MapRoute }/>
						<Route path="/jobs" component={ JobsRoute }/>

						<Redirect exact from='/tags' to='/menu'/>
						<Redirect exact from='/tags/jobs' to='/menu'/>
						<Redirect exact from='/tags/companies' to='/menu'/>
						<Route path="/tags/jobs/:tag" component={ TagsJobs }/>

						<PrivateRoute exact path="/add/job" component={ AddJob }/>

						<PublicRoute path="/register" component={ Register }/>
						<PublicRoute path="/login" component={ Login }/>
						<PrivateRoute path="/logout" component={ Logout }/>
						<PrivateRoute path="/account" component={ Account }/>
						<Route component={NoMatchRoute}/>
					</Switch>
				</main>
			</div>
		)
	}
}

export default withUser(App)
