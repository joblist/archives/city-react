import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeJob from './serializers/job';

function buildRef(props) {
	return firebase.firestore()
		.collection('jobs')
		.doc(props.match.params.id)
}

export default withFirebase(
	buildRef,
	serializeJob
);
