import React from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch';
import Root from './Root';

import './styles/reset.css';
import './styles/index.css';
import './styles/utilities.css';
import './styles/layout.css';
import './styles/button.css';
import './styles/form.css';
import './styles/nav.css';
import './styles/contextualToggle.css';
import './styles/notification.css';
import './styles/intro-text.css';
import './styles/tag.css';
import './styles/card.css';
import './styles/loading.css';
import './styles/map.css';

import 'leaflet/dist/leaflet.css'

import firebase from 'firebase/app';
import 'firebase/functions';

// fix leaflet default icon issue
import L from 'leaflet';
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

/* the firebase firestore database config */
let config;

/* the initial instance tags, used by default for all posts */
let instanceTags = [];

if (process.env.NODE_ENV !== "production") {
	config = {
		"projectId": "joblistberlin-staging",
		"apiRootUrl": "https://joblistberlin-staging.firebaseio.com",
		"apiKey": "AIzaSyAOWDGWR6dgJXNvG_B9A6hIaJNVQBwg0jI",
		"databaseURL": "joblistberlin-staging.firebaseio.com",
		"authDomain": "joblistberlin-staging.firebaseapp.com",
		"storageBucket": "joblistberlin-staging.appspot.com"
	}
} else {
	config = {
		"projectId": "joblistberlin",
		"apiRootUrl": "https://joblistberlin.firebaseio.com",
		"apiKey": "AIzaSyACEEOsMA6xpUV4uzgbQtK8T7GNLOdQRfQ",
		"databaseURL": "joblistberlin.firebaseio.com",
		"authDomain": "joblistberlin.firebaseapp.com",
		"storageBucket": "joblistberlin.appspot.com"
	}
}

console.log('process.env', process.env)

if (process.env.REACT_APP_INSTANCE_TAGS) {
	instanceTags = process.env.REACT_APP_INSTANCE_TAGS.split(' ')
}

// initialize firebase app
firebase.initializeApp(config);
// initialize firebase functions
// firebase.functions();

ReactDOM.render(
	<Root instanceTags={ instanceTags } />,
  document.getElementById('root')
);
