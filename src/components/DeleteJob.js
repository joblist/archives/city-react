import React from 'react';
import { Link } from 'react-router-dom';
import withJob from '../withJob';
import withNotification from './withNotification';
import JobCard from './JobCard';
import IntroText from './IntroText';
import { deleteJob } from '../actions/jobs';

function DeleteJob(props) {
	function handleDelete() {
		const { addNotification } = props;

		return deleteJob(props.data.id)
			.then(test => {
				const { history } = props;
				addNotification('Sucess! Job deleted.')
				history.push('/account');
			}).catch(error => {
				addNotification('Error, job not deleted. Contact an admin')
			})
	}

	if (!props.data || !props.data.id) return false

	return (
		<div>
			<IntroText center={true}>
				Are you sure you want to permanently <strong>delete</strong> this job?<br/>
				<Link to={`/jobs/${props.data.id}`} className="Nav-item Button Button--color3 mr-1">Cancel</Link>, keep the job online!
			</IntroText>
			<section className="Section">
				<JobCard data={props.data}/>
			</section>
			<IntroText center={true}>
				{props.data.isPublished ? (
					<p><strong>This job is published</strong>. <Link to={`/jobs/${props.data.id}/unpublish`} className="Nav-item Button Button--color1 mr-1">Unpublish it first</Link> to be able to delete it.</p>
				) : (
					<div>
						<p>Yes, <button className="Button Button--color1" onClick={ handleDelete }>Delete job</button> — no way back!</p>
					</div>
				)}
		</IntroText>
			<section className="Section Note">
			<ul>
			<li>Deleting a job <strong>removes it forever</strong> from the database.</li>
			{props.data.isPublished && (
				<li>Before deleting a published job, you could let it unpublish for a while and <strong>let people know</strong> that you're not looking anymore for this position.</li>
			)}
			</ul>
			</section>
		</div>
	)
}

export default withNotification(withJob(DeleteJob))
