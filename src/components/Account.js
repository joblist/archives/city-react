import React from 'react';
import { Route, NavLink } from 'react-router-dom';
import UserSettings from './UserSettings';
import UserJobs from './UserJobs';
import IntroText from './IntroText';

export default function Account(props) {
	const { match } = props;
	return (
		<div className="Page App-grow">
			<IntroText center>
					<h1>Account</h1>
			</IntroText>
			<section className="Section">
				<nav className="Nav Nav--secondary Nav--horizontal">
					<NavLink className="Nav-item" exact to={`${match.url}`}>My jobs</NavLink>
					<span className="Nav-item">—</span>
					<NavLink className="Nav-item" to={`${match.url}/settings`}>Settings</NavLink>
					<NavLink className="Nav-item" to="/logout">Logout</NavLink>
				</nav>
			</section>

			<section className="Section">
				<Route exact path={`${match.url}`} component={ UserJobs }/>
				<Route exact path={`${match.url}/settings`} component={ UserSettings }/>
			</section>
		</div>
	)
}
