import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import withPublishedJobs from '../withPublishedJobs';
import { Link } from 'react-router-dom';
import JobCard from './JobCard';
import IntroText from './IntroText';

class Jobs extends Component {

	showJobs(data) {
		if (data && data.length) {
			return (
				<div>
					{ data.map((job, index) => <JobCard summary={true} key={ index } data={ job }/>).reverse()}
				</div>
			)
		} else {
			return (
				<div className="Screen">
					<p>There are no public job offers today.</p>
				</div>
			)
		}
	}

	render() {
		const {data} = this.props

		return (
			<div className="Jobs">
				<Helmet>
          <title>Job offers — Job List City</title>
          <meta
					name="description"
					content="Job opportunities, companies currently hirring."/>
        </Helmet>

				<section className="Section">
					<nav className="Nav Nav--secondary Nav--horizontal">
						<Link to="/map/jobs" className="Nav-item Button Button--style1" title="Show the jobs on a map">Map</Link>
						<Link to="/add/job" className="Nav-item Button Button--style2" title="Publish a new job on the homepage">New job</Link>
					</nav>
				</section>

				<div className="Section">
					{data && data.length ? (
						data.map((job, index) => <JobCard summary={true} key={ index } data={ job }/>).reverse()
					) : (
						<div className="Screen">
							<p>There are no publicly visible jobs.</p>
						</div>
					)}
			  </div>
			</div>
		)
	}
}

export default withPublishedJobs(Jobs)
