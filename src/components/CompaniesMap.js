import React, { Component } from 'react'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import { Link } from 'react-router-dom'
import withPublishedCompaniesMap from '../withPublishedCompaniesMap'

class CompaniesMap extends Component {
  state = {
    defaultLat: 52.49221,
    defaultLong: 13.43479,
    defaultZoom: 10
  }

	showItem = (data) => {
		if (data && data.length) {
			return data.map(item => {
				if (item.position && item.position._lat && item.position._long) {
					const markerPosition = [item.position._lat, item.position._long]
					return (
						<Marker position={markerPosition} key={item.id}>
							<Popup>
								<Link className="" to={`/companies/${item.id}`}>
									{item.title || 'Untitled'}
								</Link>
							</Popup>
						</Marker>
					)
				} else {
					return false
				}
			})
		}
	}

  render() {
		const position = [this.state.defaultLat, this.state.defaultLong]
    return (
      <Map center={position} zoom={this.state.defaultZoom} className="Map">
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
					/>
        {this.showItem(this.props.data)}
      </Map>
    )
  }
}

export default withPublishedCompaniesMap(CompaniesMap)
