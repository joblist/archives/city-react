import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from 'firebase/app';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import withJob from '../withJob';
import IntroText from './IntroText';
import { editJob } from '../actions/jobs';

class EditJob extends Component {
  constructor() {
		super();
		this.state = {
			// page specific
			centerLat: null,
			centerLong: null,
			defaultLat: 52.49221,
			defaultLong: 13.43479,
			defaultZoom: 12,

			// job model properties
			id: '',
	    title: '',
			body: '',
			url: '',
			jobLat: null,
			jobLong: null
		};
  }

	componentDidUpdate(prevProps) {
		if (!this.props || !this.props.data) return
		const { id } = this.props.data;
		let prevId;

		if (prevProps && prevProps.data) {
			prevId = prevProps.data.id;
		}

		if (id !== prevId) {
			this.setInitialData(this.props)
		}
	}

	setInitialData(props) {
		if (!props || !props.data) return
		const {id, title, body, url, position} = props.data;
		const {_lat: jobLat, _long: jobLong} = position || {}
		this.setState({
			title,
			body,
			url,
			id,
			jobLat,
			jobLong
		})
	}

	onViewportChanged = (viewport) => {
    this.setState({
			centerLat: viewport.center[0],
			centerLong: viewport.center[1]
		})
  }

	setMapPosition = () => {
		this.setState({
			jobLat: this.state.centerLat,
			jobLong: this.state.centerLong
		})
	}

	removeMapPosition = () => {
		this.setState({
			jobLat: null,
			jobLong: null
		})
	}

  handleSubmit = (e) => {
		e.preventDefault();
		const { history } = this.props;
		const { id, title, body, url, jobLat, jobLong } = this.state;

		let position = null
		if (jobLat && jobLong) {
			position = new firebase.firestore.GeoPoint(jobLat, jobLong)
		}

		editJob({
			id,
			title,
			body,
			url,
			position
		}).then(() => {
			history.push(`/jobs/${id}`);
		}).catch(error => {
			console.log('Edit job submit error:', error);
		})
  }

  handleChange = (e) => {
		// if it is a URL fetch and set title
		this.setState({
	    [e.target.name] : e.target.value
		});
  }


  render() {

		const { id, body, title, url } = this.state;
		const isPublished = this.props.data && this.props.data.isPublished;

		const {
			defaultLong,
			defaultLat,
			jobLong,
			jobLat
		} = this.state
		let position = [defaultLat, defaultLong]
		const jobHasPosition = jobLat && jobLong
		const markerPosition = [jobLat, jobLong]
		if (jobHasPosition) {
			position = markerPosition
		}

		return (
			<div>
				<IntroText center>
					<h2>Edit job</h2>
					<p>Update the job informations.</p>
				</IntroText>
				<section className={`Card Card--job ${isPublished && 'Card--isPublished'}`}>
					<form onSubmit={ this.handleSubmit }>
						<article>
							<label>
								<strong>Title</strong>
								<input name="title"  type="text" placeholder="Job's title" onChange={ this.handleChange } value={ title }/>
								<p>A clear and short title for this job offer.</p>
							</label>
						</article>

						<article>
							<label>
								<strong>URL <small>(optional)</small></strong>
								<input name="url" type="url" placeholder="Link to the job offer" onChange={ this.handleChange } value={ url }/>
								<p>You can use this field to link anywhere on the internet!</p>
							</label>
						</article>

						<article>
							<label>
								<strong>Geographic position</strong>
								<Map
									className="Map Map--add"
									center={position}
									zoom={this.state.defaultZoom}
									onViewportChanged={this.onViewportChanged}>
									<TileLayer
										attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
										url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
										/>
									{ jobHasPosition ? (
										<Marker position={markerPosition}>
											<Popup>
												Job position
											</Popup>
										</Marker>
									) : (<></>)}
			</Map>
				</label>
				<nav className="Nav Nav--horizontal Nav--right">
				{ jobHasPosition ? (
					<button className="Button Button--color1 Nav-item" type="button" onClick={this.removeMapPosition}>Remove position</button>
				) : (<></>)}
				<button className="Button Button--color1 Nav-item" type="button" onClick={this.setMapPosition}>Set job position</button>
				</nav>
			</article>

						<article>
							<label>
								<strong>Description</strong>
								<textarea name="body" type="text" placeholder="Job's description" onChange={ this.handleChange } value={ body }/>
								<p>
									Write everything a candidate needs to know to see if they match your needs.<br/>
									You can describe the position, how to apply, what skills your are looking for.
								</p>
							</label>
						</article>
						<nav className="Nav Nav--horizontal Nav--right">
							<Link to={`/jobs/${id}`} className="Nav-item Button Button--color3 Button--back mr-1">Cancel</Link>
							<button className="Button Button--color2" type="submit">Save</button>
						</nav>
					</form>
				</section>
			</div>
		)
  }
}

export default withJob(EditJob);
