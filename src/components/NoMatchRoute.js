import React from 'react';
import { Link } from 'react-router-dom';

export default function NoMatchRoute() {
	return(
		<section className="Page Note">
			<p>☯</p>
			<p>404 - page not found</p>
			<p>
				Maybe <Link to="/info">get in touch</Link>?
			</p>
			<p>☮️</p>
		</section>
	)
}
