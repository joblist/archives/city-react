import React from 'react';
import { Link } from 'react-router-dom';

function getClasses(model, isSummary) {
	const {isPublished} = model
	return `
Card
Card--job
${isPublished ? 'Card--isPublished' : ''}
${isSummary ? 'Card--isSummary' : ''}
`
}


function JobCard({data, mini, full, summary, hideTitle}) {
	if(!data) return <></>;
	const { id, url, daysLeft, tags } = data;
	const bodyLimit = 500
	let { title, body } = data;
	// remove unecessary white spaces
	title = title.trim()

	// short version of the body from summary props
	if (summary && body.length > bodyLimit ) {
		body = body.slice(0, Number(summary) > bodyLimit || bodyLimit) + ' ...'
	}

	const hasAnyInfo = title || url || daysLeft || body
	const noDetails = hasAnyInfo || !url || !body

	return (
		<article className={ getClasses(data, summary) }>

			{ !hideTitle && (
				<Link className="Card-header" to={`/jobs/${id}`}>
					<h3 className="Card-title">{title || 'Untitled'}</h3>
				</Link>
			)}

		{!hasAnyInfo && <p><i>This job has not yet any information.</i></p> }
		{ title && !body && !url && <p><i>This job has no detailed information.</i></p> }

		{ !mini && url && <a href={url} className="Card-url"><input disabled className="Input Input--transparent"value={url}/></a> }
		{ !mini && body && <div className="Card-body">{body}</div> }
    { !mini && (
      <div className="Card-tags">
				{ tags && tags.length ? (
					tags.map((tag, index) => <Link key={index} to={`/tags/jobs/${tag}`} className="Card-tag">{tag}</Link>)
				) : ''}
			{daysLeft ? (
				<span
					className="Card-tag Card-timeLeft"
					title="Number of days untill this job disappears from the homepage">
					{daysLeft} {daysLeft >= 0 ? '☀' :'🌒'}</span>
			) : (
				<span
					className="Card-tag Card-timeLeft"
					title="A published job appears on the homepage 𓃞 ">Not published</span>
			)}
      </div>
    ) }
		</article>
	)
}

export default JobCard
