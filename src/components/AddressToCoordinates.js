import React, {useState} from 'react'

export default ({onSearch}) => {

	const [search, setSearch] = useState('')
	const [coordinates, setCoordinates] = useState({})

	const handleChange = (event) => {
		event.preventDefault()
		setSearch(event.target.value)
	}

	const handleSearch = async (search) => {
		const res = await fetch(`https://nominatim.openstreetmap.org/search?q=${search}&format=json&polygon=1&addressdetails=1`)
		const data = await res.json()
		const coord = data[0]
		setCoordinates(coord)
		onSearch(coord)
	}

	return (
		<label class="Label">
			<i>Search by address (or companies on <a href="https://www.onosm.org/" target="_blank">OSM</a>)</i>
			<div class="Label-group">
				<input
					onChange={handleChange}
					value={search}
				type="text"
				placeholder="Position the company from its address" />
				<aside className="ButtonGroup ButtonGroup--right">
					<button type="button" class="Button" onClick={() => handleSearch(search)}>Search</button>
				</aside>
			</div>
		</label>
	)
}
