import React from 'react';
import IntroText from './IntroText';

export default () => {
	return (
		<div className="Page Screen">
			<IntroText center>
				<h1>Info</h1>
			</IntroText>
			<div className="Info">
				<IntroText>
					<i>Job List City</i> is a <strong>tool made for people</strong> looking to work together.
				</IntroText>
			</div>
			<nav className="Nav Nav--horizontal Screen">
				<a className="Nav-item Button Button--color1" href="mailto:info@joblist.city" target="_blank" rel="noopener noreferrer">Email</a>
				<a className="Nav-item Button Button--color1" href="https://www.facebook.com/joblistberlin" target="_blank" rel="noopener noreferrer">Facebook</a>
				<a className="Nav-item Button Button--color1" href="https://www.linkedin.com/company/joblistberlin" target="_blank" rel="noopener noreferrer">Linkedin</a>
			</nav>
		</div>
	)
}
