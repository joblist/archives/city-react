import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from "react-helmet";
import withCompanies from '../withCompanies';
import Loading from './Loading';
import CompanyCard from './CompanyCard';
import IntroText from './IntroText';
import Search from './Search';
import { parse } from 'query-string';

class Companies extends Component {
	constructor() {
		super();

		this.state = {
			search: ''
		}
	}

	handleSearch = (value) => {
		this.setState({ search: value })
		// Update query param in URL as well.
		const location = { search: `?search=${window.encodeURIComponent(value)}` }
		this.props.history.replace(location)
	}

	applySearch = (company) => {
		return this.buildSearchPool(company)
			.toLowerCase()
			.includes(this.state.search.toLowerCase())
	}

	clearSearch = () => {
		this.handleSearch('')
	}

	buildSearchPool(company) {
		return company.title + company.body
	}

	componentWillMount(props) {
		this.setSearchFromURL(parse(this.props.location.search))
	}

	componentWillReceiveProps(nextProps) {
		let current = this.props.location.search
		let next = nextProps.location.search || ''

		if (current !== next) {
			this.setSearchFromURL(parse(next))
		}
	}

	setSearchFromURL({ search }) {
		if (!search) search = ''
		this.setState({ search: window.decodeURIComponent(search) })
	}

	render() {
		if (!this.props.data) return <Loading />
		const length = this.props.data.length
		return (
			<div>
				<Helmet>
					<title>Companies hiring — Job List City</title>
					<meta
						name="description"
						content="A list of companies hiring, and a direct link to their career page. Job List City also lists job boards and other websites, making it clear, simple and fast to find work opportunities arround the world." />
				</Helmet>

				<IntroText center>
					<h1>Companies</h1>
					Hi! All <strong>{length}</strong> companies in this list have <strong>offices in Berlin</strong>, and <strong>job offers</strong> on their website.
			</IntroText>
				<section className="Section">
					<nav className="Nav Nav--secondary Nav--horizontal">
						<Link className="Nav-item Button Button--style1" to='/map/companies'>Map</Link>
						<Link className="Nav-item Button Button--style1" to='companies/tags'><strong> #tags</strong></Link>
						<Link className="Nav-item Button Button--style2" to='/add/company'>New</Link>
					</nav>
				</section>

				{length ? (
					<Search
						search={this.state.search}
						handleSearch={this.handleSearch}
						clearSearch={this.clearSearch}
					/>
				) : (
						''
					)}

				<section className="Section Container">
					{
						length ? (
							this.props.data
								.filter(this.applySearch)
								.map((company, index, companies) => (
									<CompanyCard
										key={company.id}
										data={company}
									/>
								)).reverse()
						) : (
								<p>There are no companies yet.</p>
							)
					}
				</section>
			</div>
		)
	}
}

export default withCompanies(Companies);
