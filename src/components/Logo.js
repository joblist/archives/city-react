import React from 'react';

export default () => {
	return (
		<img
			className="Logo-img"
			src={ process.env.PUBLIC_URL + '/jlb-logo.png' }
			alt="Home"
			/>
	)
}
