import React from 'react';
import {Helmet} from 'react-helmet';
import { NavLink } from 'react-router-dom';
import IntroText from './IntroText';
import { isAuthenticated } from '../actions/auth';
import Logo from './Logo';

export default () => {
	const auth = isAuthenticated();
	return (
		<div className="Page Screen">
			<Helmet>
        <title>Menu — Job List City</title>
        <meta
					name="description"
					content="Navigation menu, to explore the pages of this site."/>
      </Helmet>

				<nav className="Nav Nav--logo">
						<NavLink className="Nav-item Logo" to="/">
							<h1 title="Job List City">
								<Logo title="Home"/>
							</h1>
						</NavLink>
						<NavLink className="Nav-item" to="/menu" title="Menu of the pages of this site. A good start of navigation.">
							<h1>Menu</h1>
						</NavLink>
					</nav>

				<aside className="Info">
					<nav className="Nav Nav--main Nav--vertical">
						<NavLink className="Nav-item" to="/jobs" title="List of jobs currently open.">Jobs</NavLink>
						{ auth && <NavLink className="Nav-item" to="/account" title="Your user account.">Account</NavLink> }
						{ !auth && <NavLink className="Nav-item" to="/login" title="Log into your user account.">Login</NavLink> }
						<NavLink className="Nav-item" to="/info" title="☮ What is this page about?">Info</NavLink>
					</nav>
				</aside>
		</div>
	)
}
