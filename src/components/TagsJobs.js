import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import withJobTag from '../withJobTag';
import { Link } from 'react-router-dom';
import JobCard from './JobCard';
import IntroText from './IntroText';

class TagsJobs extends Component {

	showJobs(data) {
		const currentTag = this.props.match.params.tag
		if (data && data.length) {
			return (
				<div>
					<p className="ButtonGroup ButtonGroup--right">
						<Link to="/add/job" className="Button Button--style2">New job</Link>
					</p>
					<div>
						{ data.map((job, index) => <JobCard summary={true} key={ index } data={ job }/>).reverse()}
					</div>
				</div>
			)
		} else {
			return (
				<div className="Screen">
					<p>There are currently no published job offers for #<strong>{currentTag}</strong>.</p>
					<p>Explore <Link to="/jobs">all job offers</Link>.</p>
				</div>
			)
		}
	}

	render() {
		const currentTag = this.props.match.params.tag
		return (
			<div className="Page Jobs">
				<Helmet>
          <title>{currentTag} job offers — Job List City</title>
          <meta
						name="description"
						content={`Job offers for the hashtag #${currentTag}`}/>
        </Helmet>

				{this.props.data && this.props.data.length ? (
					<IntroText center>
						<h1>Job offers for #<i>{currentTag}</i></h1>
						You can also explore <Link to="/jobs" className="Button Button--color1">all jobs</Link>
					</IntroText>
				) : ''}

				<div className="Section">
					{ this.showJobs(this.props.data) }
			  </div>
			</div>
		)
	}
}

export default withJobTag(TagsJobs)
