import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import withCompanyTag from '../withCompanyTag';
import { Link } from 'react-router-dom';
import CompanyCard from './CompanyCard';
import IntroText from './IntroText';

class TagsCompanies extends Component {

	showCompany(data) {
		if (data && data.length) {
			return (
				<div>
					<div>
						{ data.map((job, index) => <CompanyCard key={ index } data={ job }/>).reverse()}
					</div>
				</div>
			)
		} else {
			return (
				<div className="Screen">
					<p>There are currently no published job offers for this <strong>#tag</strong>.</p>
					<p>Explore <Link to="/jobs">all job offers</Link> and <Link to="/companies">companies</Link> curently hirring.</p>
				</div>
			)
		}
	}

	render() {
		const currentTag = this.props.match.params.tag
		return (
			<div className="Page Jobs">
				<Helmet>
          <title>Companies hirring {currentTag} — Job List City</title>
          <meta
						name="description"
						content="Job opportunities; all companies on Job List City are currently hirring."/>
        </Helmet>

				{this.props.data && this.props.data.length ? (
					<IntroText center>
						<h1>Companies in <i>#{currentTag}</i></h1>
						You can also explore <Link to="/companies" className="Button Button--color1">all companies</Link>
					</IntroText>
				) : ''}

				<div className="Section">
					{ this.showCompany(this.props.data) }
			  </div>
			</div>
		)
	}
}

export default withCompanyTag(TagsCompanies)
