import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { Link } from 'react-router-dom';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import CompanyCard from './CompanyCard';
import withCompany from '../withCompany';
import Error from './Error';
import Loading from './Loading';

class Company extends Component {
	render() {
		if(this.props && this.props.error) return <Error error={this.props.error}/>
		if(!this.props || !this.props.data) return <Loading/>

		const { data } = this.props;
		const {title, url, body, isPublished, position} = data
		const defaultZoom = 11

		const mapUrl = 'https://www.google.com/maps/search/'
		+ encodeURIComponent(`${title} berlin germany`)
		+ '/data=!5m1!1e3'

		let hasPosition = position && position._lat && position._long && true
		let markerPosition

		if (hasPosition) {
			markerPosition = [position._lat, position._long]
		}


		return (
		<article>
			<Helmet>
        <title>{title} - Careers</title>
        <meta
				name="description"
				content={ `Job opportunities @ ${title} — ${body}` } />
      </Helmet>

			<CompanyCard data={data}/>

			{ hasPosition ? (
					<article>
						<Map
							className="Map Map--small"
							center={markerPosition}
							zoom={defaultZoom}>
							<TileLayer
							attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
							url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
							/>
							<Marker position={markerPosition}>
								<Popup>
									{title}
								</Popup>
							</Marker>
						</Map>
					</article>
			) : (
					<p className="ButtonGroup ButtonGroup--right">
						<a className="Button Button--color2" href={mapUrl}>View on a map</a>
					</p>
			)}

			<section>
				<p>&larr; back to <Link to="/">all companies</Link></p>
			</section>
		</article>
		)
	}
}

export default withCompany(Company);
