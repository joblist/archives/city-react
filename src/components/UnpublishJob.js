import React from 'react';
import { Link } from 'react-router-dom';
import withJob from '../withJob';
import JobCard from './JobCard';
import IntroText from './IntroText';
import { unpublishJob } from '../actions/jobs';

function UnpublishJob(props) {
	function handleUnpublish() {
		return unpublishJob(props.data.id)
			.then(test => {
				const { history } = props;
				history.push(`/jobs/${props.data.id}`);
			}).catch(error => {

			})
	}

	if (!props.data || !props.data.id) return false

	return (
		<div>

			<IntroText center={true}>
				Are you sure you want to permanently  <strong>unpublish</strong> this job?<br/>
				<Link to={`/jobs/${props.data.id}`} className="Nav-item Button Button--color3 mr-1">Cancel</Link>, keep the job published on the homepage!
		</IntroText>

			<section className="Section">
				<JobCard mini={true} data={props.data}/>
			</section>

			<IntroText center={true}>
			Yes, <button className="Button Button--color1" onClick={ handleUnpublish }>unpublish job</button> &larr; no way back.
			</IntroText>

			<section>
			<details>
			<summary>
			Info
		  </summary>
			<ul>
			<li>When a job is unpublished, it is <strong>no more discoverable</strong> from the homepage.</li>
			<li>You <strong>can edit this job</strong> without unpublishing it.</li>
			<li>An unpublished job will <strong>need to go through the publication steps again</strong> in order to be re-published.</li>
			<li>An unpublished job is <strong>still accesible publicly from its URL</strong>. It disapears completely when you choose to delete it.</li>
			<li>A job <strong>disapears completely when you delete it</strong>.</li>
			</ul>
			</details>
			</section>
		</div>
	)
}

export default withJob(UnpublishJob)
