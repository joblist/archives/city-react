import React, {Component} from 'react';
import withCompaniesUnpublished from '../withCompaniesUnpublished';
import Loading from './Loading';
import CompanyCard from './CompanyCard';
import IntroText from './IntroText';

class CompaniesUnpublished extends Component {
	render() {
		if(!this.props.data) return <Loading/>
		const length = this.props.data.length
		console.log('this.props.data', this.props.data)
		return (
			<div>
				<IntroText center>
					{ length ? (
						'These companies are not yet aproved and published on the homepage.'
						) : (
							'No companies to review.'
					)}
				</IntroText>

				<div className="Companies">
				{
					length ? (
						this.props.data
							.map((company, index, companies) => (
								<CompanyCard
									key={ company.id }
									data={ company }
									/>
							)).reverse()
					) : (
						<p>There are no companies yet.</p>
					)
				}
				</div>
			</div>
		)
	}
}

export default withCompaniesUnpublished(CompaniesUnpublished);
