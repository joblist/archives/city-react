import React, { Component } from 'react';
import withNotification from './withNotification';
import IntroText from './IntroText';
import { getCurrentUser,
				 sendVerificationEmail,
				 updateUserEmail,
				 sendPasswordResetEmail } from '../actions/auth';

class UserSettings extends Component {
	constructor() {
		super();
		this.state = {
			email: null,
			emailVerified: null,
			newEmail: null
		}
	}

	componentDidMount() {
		this.reloadUser();
	}

	isEmailVerified() {
		const { email, emailVerified} = this.state;
		if (emailVerified) {
			return `(verified) ${email}`;
		} else {
			return `(not-verified) ${email}`;
		}
	}

	handleEmailUpdate = (e) => {
		this.setState({
	    newEmail : e.target.value
		});
	}

	sendEmail = () => {
		const { addNotification } = this.props;
		sendVerificationEmail().then(() => {
			addNotification('A confirmation email was sent to your email address. Check your inbox');
		})
	}
	reloadUser() {
		return getCurrentUser().then(user => {
			const {email,
						 emailVerified } = user;
			this.setState({
				email,
				emailVerified,
				newEmail: ''
			});
		});
	}

	updateEmail = () => {
		const { addNotification } = this.props
		updateUserEmail(this.state.newEmail).then(() => {
			addNotification('A confirmation email was sent to both your new and old email. Check your inbox!');
			this.reloadUser();
		}).catch(e => {
			addNotification(e.message);
		})
	}

	changePassword = () => {
		const { addNotification } = this.props;
		sendPasswordResetEmail(this.state.email).then(() => {
			addNotification('A reset link has been sent to your email adress.');
		}).catch(e => {
			addNotification(e.message);
		})
	}

	render() {
		if(!this.state.email) return null;
		const { email, newEmail, emailVerified } = this.state;

		return (
			<div className="">
				<IntroText center>
					<p><strong>Customization</strong> of your settings.</p>
				</IntroText>
				<section className="Section">
					<form>
						<label>
							<h2>Email</h2>
							<p>
								The email address that <strong>you use to login</strong> to this website.<br/>
								Enter a new email if you would like to change it.
							</p>
							<input title={ email }
										 type="email"
										 value={ newEmail }
										 onChange={this.handleEmailUpdate }
										 placeholder={ this.isEmailVerified() }/>
						</label>
					</form>
					<div>
							{ newEmail && <button className="Button" onClick={ this.updateEmail }>Update email</button> }
							{ !emailVerified && !newEmail && (
								<div>
									<p><strong>Verify your email address to confirm your account.</strong></p>
									<button className="Button Button--color1" onClick={ this.sendEmail }>Re-send verification email</button>
								</div>
							) }
					</div>
				</section>
				<section className="Section">
				  <h2>Password</h2>
				  <p>Use a <strong>strong</strong> password to protect access to your account.</p>
					<button className="Button Button--color1" onClick={ this.changePassword }>Change password</button>
				</section>
			</div>
		)
	}
}

export default withNotification(UserSettings);
