import React, { Component } from 'react'
import withNotification from './withNotification'
import firebase from 'firebase/app'
import { postCompany } from '../actions/companies'
import AddressToCoordinates from './AddressToCoordinates'
import IntroText from './IntroText'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'

class AddCompany extends Component {
  constructor() {
		super();
		this.state = {
			// page specific
			centerLat: null,
			centerLong: null,
			defaultLat: 52.49221,
			defaultLong: 13.43479,
			defaultZoom: 12,

	    url: '',
	    title: '',
			body: '',
			companyLat: null,
			companyLong: null
		};
  }

	handleSearch = (data) => {
		console.log(data)
		if (!data) return

		const {lat, lon} = data
		this.setState({
			companyLat: lat,
			companyLong: lon,
			defaultLat: lat,
			defaultLong: lon,
		})
	}

  handleSubmit = (e) => {
		e.preventDefault();
		const { addNotification, history } = this.props;
		const { url, body, centerLat, centerLong  } = this.state;
		let { title } = this.state

		const latitude = centerLat
		const longitude = centerLong

		// remove unecessary white spaces
		title = title.trim()

		let position
		if (latitude && longitude) {
			position = new firebase.firestore.GeoPoint(latitude, longitude)
		}

		const company = {
			title,
			body,
			url,
			position: position || null
		}

		postCompany(company).then(newModel => {
			console.log('addcompany:submit sucess');
			addNotification(`The company ${title} has been submitted for review.`);
			history.push('/companies');
		}).catch(error => {
			console.log('addCompany:submit error:', error);
			addNotification(`error adding the company: ${error}`);
		})
  }

  handleChange = (e) => {
		this.setState({
	    [e.target.name] : e.target.value
		});
  }

	onViewportChanged = (viewport) => {
    this.setState({
			centerLat: viewport.center[0],
			centerLong: viewport.center[1]
		})
  }

	setMapPosition = () => {
		this.setState({
			companyLat: this.state.centerLat,
			companyLong: this.state.centerLong
		})
	}

	removeMapPosition = () => {
		this.setState({
			companyLat: null,
			companyLong: null
		})
	}


  render() {

		const {
			body,
			title,
			url,
			defaultLong,
			defaultLat,
			companyLong,
			companyLat
		} = this.state

		const position = [defaultLat, defaultLong]
		const companyHasPosition = companyLat && companyLong
		const markerPosition = [companyLat, companyLong]


		return (
			<div className="Page">
				<section className="Section AddCompany">
					<IntroText center>
						<h2>New company</h2>
						<p>Submit a new company to Job List City</p>
					</IntroText>

					<form onSubmit={this.handleSubmit} className="">
						<article>
							<label>
								<strong>Name</strong>
								<input required name="title"  type="text" placeholder="Company name" onChange={ this.handleChange } value={ title }/>
							</label>
						</article>

						<article>
							<label>
								<strong>Url</strong>
								<input required name="url" type="url" placeholder="Company's website, job listing page" onChange={ this.handleChange } value={ url } title="https://...your-company.com/jobs"/>
							</label>
						</article>

						<article>
							<label>
								<strong>Geographic position</strong>
								<p>You can position the company on the map to help people find it.</p>
								<AddressToCoordinates onSearch={this.handleSearch}/>
								<Map
									className="Map Map--add"
									center={position}
									zoom={this.state.defaultZoom}
									onViewportChanged={this.onViewportChanged}>
									<TileLayer
									attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
									url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
									/>
									{ companyHasPosition ? (
											<Marker position={markerPosition}>
												<Popup>
													Company position
												</Popup>
											</Marker>
									) : (<></>)}</Map>
							</label>

							<aside className="ButtonGroup">
								{ companyHasPosition ? (
										<button type="button" className="Button Nav-item" type="button" onClick={this.removeMapPosition}>Remove position</button>
								) : (
										<button type="button" className="Button Button--color1 Nav-item" type="button" onClick={this.setMapPosition}>Set position manually</button>
								)}
							</aside>
						</article>

						<article>
							<label>
								<strong>Description</strong>
								<textarea name="body" type="text" maxLength="999" placeholder="What is this company about? #startup #bar #jobBoard ..." onChange={ this.handleChange } value={ body } title="Write a little description about this Company, what it does, or what type of jobs it offers (max. 1000 characters)"></textarea>
							</label>
						</article>
						<div className="ButtonGroup ButtonGroup--right">
							<button className="Button Button--color3" type="submit">Submit company</button>
						</div>
					</form>
				</section>

				<section className="Section Note">
					<ul>
						<li><strong>URL</strong>s should lead directly to a <strong>job</strong> or <strong>career</strong> page</li>
						<li>All submissions are <strong>reviewed</strong> before publication</li>
					</ul>
				</section>
			</div>
		)
  }
}

export default withNotification(AddCompany);
