import React from 'react';
import { Link } from 'react-router-dom';
import withCompanies from '../withCompanies';
import CompaniesTags from './CompaniesTags';
import Loading from './Loading';

const CompaniesTagsRoute = (props) => {
	const companies = props.data
	if (!companies) {
		return <Loading/>
	}
	if (!companies.length) {
		return (
			<div className="Screen">
				<p>There are no tags.</p>
				<Link to="/companies" className="Nav-item Button Button--color3 Button--back mr-1">go back to the list of companies</Link>
			</div>
		)
	}
	return !companies ? <Loading/> : (
		<div>
			<nav className="ButtonGroup ButtonGroup--right">
				<Link to="/companies" className="Nav-item Button Button--color3 Button--back mr-1">go back to the list of companies</Link>
			</nav>
			<CompaniesTags companies={companies}/>
		</div>
	)
}

export default withCompanies(CompaniesTagsRoute)
