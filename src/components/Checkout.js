import React from 'react'
import StripeCheckout from 'react-stripe-checkout';

export default class Checkout extends React.Component {
  render() {
		const {amount, description, onToken} = this.props;
		// TODO: replace stripe key by live/test depending onenvironment
    return (
      <StripeCheckout
        stripeKey="pk_test_yj59oGsyjErsaXa5TGUAKKlq"
				ammount={amount}
				name="Job List"
				description={description}
				image={process.env.PUBLIC_URL + '/jlb-logo.png'}
				locale="auto"
        token={onToken}>

				<button className="Button Button--color4">{ this.props.children || 'Pay!'}</button>

			</StripeCheckout>
    )
  }
}
