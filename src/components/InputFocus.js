import React, { Component } from 'react'

class InputFocus extends Component {
	constructor(props) {
		super(props)
		// create a ref to store the editor DOM element
		this.inputRef = React.createRef()
	}

	handleClick = event => {
		event.preventDefault()
		this.inputRef.current.select()
	}

	render() {
		const { value } = this.props
		return (
			<input
				className="InputFocus"
				ref={ this.inputRef }
				value={value}
				placeholder="You need to pass a `value` props"
				onClick={ (e) => this.handleClick(e) }
				readOnly></input>
		)
	}
}

export default InputFocus
