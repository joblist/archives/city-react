import React from 'react';
import { Link } from 'react-router-dom';
import withUserJobs from '../withUserJobs';
import JobCard from './JobCard';
import IntroText from './IntroText';

function UserJobs({data}) {

	function showJobs(data) {
		return data && data.length ? (
			<div>
				<p className="ButtonGroup ButtonGroup--right"><Link to="/add/job" className="Button Button--style2">New job</Link></p>
				{ data.map((job, index) => <JobCard key={ index } data={ job } />).reverse() }
				<p>
				All jobs you created are <strong>publicly accessible</strong> by anyone.<br/>
				Published jobs are <Link to="/jobs">discoverable from the homepage</Link>.
				</p>
			</div>
		) : (
			<div className="Screen">
				<p>You haven't created a job yet.</p>
				<IntroText center>
					You can <Link to="/add/job" className="Button Button--style2">create a new job</Link> anytime.
				</IntroText>
			</div>
		)
	}

	return (
		<>
			{ showJobs(data) }
		</>
	)
}

export default withUserJobs(UserJobs)
