import React from 'react';
import { Link } from 'react-router-dom';

export default ({data}) => {
	const {isPublished} = data
	return (
		<nav className="Nav Nav--horizontal Nav--right">

			<Link
				className="Nav-item"
				to={`/jobs/${data.id}/delete`}>Delete</Link>

			{ isPublished ? (
				<Link
					className="Nav-item"
					to={`/jobs/${data.id}/unpublish`}>Unpublish</Link>
			) : (
				<Link
					className="Nav-item"
					to={`/jobs/${data.id}/publish`}>Publish</Link>
			)}

		  <Link
				className="Nav-item"
				to={`/jobs/${data.id}/edit`}>Edit</Link>
		</nav>
	)
}
