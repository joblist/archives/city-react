import React from 'react';
import { Route } from 'react-router';
import PrivateRoute from './PrivateRoute';
import Jobs from './Jobs';
import Job from './Job';
import EditJob from './EditJob';
import DeleteJob from './DeleteJob';
import PublishJob from './PublishJob';
import UnpublishJob from './UnpublishJob';

export default function JobsRoute(props) {
	const { match } = props;

	return (
		<div className="Page">
			<Route exact path={`${match.url}`} component={ Jobs }/>
			<Route exact path={`${match.url}/:id`} component={ Job }/>
			<PrivateRoute path={`${match.url}/:id/edit`} component={ EditJob }/>
			<PrivateRoute path={`${match.url}/:id/delete`} component={ DeleteJob }/>
			<PrivateRoute path={`${match.url}/:id/publish`} component={ PublishJob }/>
			<PrivateRoute path={`${match.url}/:id/unpublish`} component={ UnpublishJob }/>
		</div>
	)
}
