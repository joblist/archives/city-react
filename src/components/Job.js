import React from 'react';
import { Helmet } from "react-helmet";
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'
import JobCard from './JobCard';
import IntroText from './IntroText';
import InputFocus  from './InputFocus';
import withJob from '../withJob';
import EditJobAdmin from './EditJobAdmin';
import userOwnsJob from '../actions/jobs';

function Job(props) {
	const {data, user} = props
	if (!data) return <></>;
	const owner = userOwnsJob(user, data)
	const {url, body, isPublished, position} = data
	const defaultZoom = 13
	let {title} = data
	title = title || 'Untitled'

	let hasPosition = position && position._lat && position._long && true
	let markerPosition

	if (hasPosition) {
		markerPosition = [position._lat, position._long]
	}

	return (
		<div className="App-grow">
			<Helmet>
        { title && (
						<title>{title} - Job in City</title>
				)}
        <meta
				name="description"
				content="A list of companies hiring, and a direct link to their career page. Job List City also lists job boards and other websites, making it clear, simple and fast to find work opportunities."/>
      </Helmet>

      <IntroText center={true}>
				<h1>{title}</h1>
			</IntroText>

			{ owner && <EditJobAdmin data={ data }/> }

			<JobCard data={ data } hideTitle={true}/>

			{ hasPosition && (
					<article>
						<Map
							className="Map Map--small"
							center={markerPosition}
							zoom={defaultZoom}>
							<TileLayer
							attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
							url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
							/>
							<Marker position={markerPosition}>
								<Popup>
									Job position
								</Popup>
							</Marker>
						</Map>
					</article>
			)}

			{ owner && !title && (
					<p>Write a short title to help visitors find the job.</p>
			)}
			{ owner && title && !url && !body && (
					<p>
						You should add a <strong>description or a URL</strong> to this job.<br/>
						Explain what the job is about, how we should get in touch with you.
					</p>
			) }
		  { title && (
					<article>
						<details className="Help">
							<summary>
								<span>Share this job</span>
							</summary>
							<form>
								<label>
									<InputFocus value={document.URL}/>
									<p>You can use this URL to share this job anywhere you'd like. Copy and post it!</p>
								</label>
							</form>
						</details>
					</article>
			) }
		</div>
  )
}

export default withJob(Job);
