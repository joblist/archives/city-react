import React, { Component } from 'react';
import {Helmet} from 'react-helmet';
import { Link } from 'react-router-dom';
import {
	loginWithEmail,
	sendPasswordResetEmail } from '../actions/auth';
import withNotification from './withNotification';

class Login extends Component {

	constructor() {
		super();
		this.state = {
	    email: '',
			password: ''
		};
  }

  handleSubmit = (e) => {
		const { addNotification } = this.props;
		e.preventDefault();
		loginWithEmail(this.state.email, this.state.password)
			.then(() => {
				this.props.history.push('/account')
				addNotification('You are logged in!')
			})
			.catch(e => {
				addNotification(e.message)
			});
  }

  handleChange = (e) => {
		this.setState({
	    [e.target.name] : e.target.value
		});
  }

	handleReset = () => {
		const { addNotification } = this.props;
		const email = this.state.email;

		if(!email) {
			return addNotification('Type your email in the email field and click this button again');
		}

		sendPasswordResetEmail(email).then(() => {
			addNotification(`A reset link was sent to your email <${email}>`);
		}).catch(e => {
			addNotification(e.message);
		})
	}

  render() {
		const { email, password } = this.state;

		return (
			<div className="Page">
				<Helmet>
          <title>Login — Job List City</title>
          <meta
					name="description"
					content="Login to Job List City. You can also register an account for free!"/>
        </Helmet>

				<section className="Section Screen Login">
					<h2>Login to <small><Link to="/">Job List City</Link></small></h2>
					<form onSubmit={ this.handleSubmit } name="login">
						<article>
							<label>
								<strong>Email</strong>
								<input name="email"  type="email" placeholder="Email" onChange={ this.handleChange } value={ email }/>
							</label>
						</article>

						<article>
							<label>
								<strong>Password</strong>
								<input name="password" type="password" placeholder="Password" onChange={ this.handleChange } value={ password }/>
							</label>
						</article>
						<div className="ButtonGroup ButtonGroup--right">
							<button className="Button Button--color3" type="submit">Login</button>
						</div>
					</form>
					<div className="ButtonGroup ButtonGroup--right">
						<button className="Button" onClick={ this.handleReset } type="none">Reset password</button>
					</div>
				</section>

				<section className="Section Note">
					<p>Don't have an account yet? <Link to="/register" className="Button Button--color4">Register</Link></p>
				</section>
			</div>
		)
  }
}

export default withNotification( Login );
