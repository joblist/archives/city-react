import React, { Component } from 'react';
import {Helmet} from 'react-helmet';
import { Link } from 'react-router-dom';
import { registerWithEmail } from '../actions/auth';
import withNotification from './withNotification';

class Register extends Component {
	constructor() {
		super();
		this.state = {
	    email: '',
			password: ''
		};
  }

  handleSubmit = (e) => {
		e.preventDefault();
		const { history, addNotification } = this.props;
		const { email, password } = this.state;

		registerWithEmail(email, password)
			.then(() => {
				history.push('/login');
				addNotification('You are now logged in. Welcome!');
			})
			.catch(e => {
				return addNotification(e.message);
			});
  }

  handleChange = (e) => {
		this.setState({
	    [e.target.name] : e.target.value
		});
  }


  render() {
		const { email, password } = this.state;

		return (
			<div className="Page">
				<Helmet>
          <title>Register — Job List City</title>
          <meta
					name="description"
					content="Register: create an account to manage companies and jobs on Job List City."/>
        </Helmet>

				<section className="Section Screen Login">
					<h2>Register to<small> <Link to="/">Job List City</Link></small></h2>
					<p>
						Fill in the following informations to <strong>create a new account</strong>.
						With an account you can <strong>publish jobs</strong>.
					</p>
					<form onSubmit={ this.handleSubmit } name="login">
						<article>
							<label>
								<strong>Email</strong>
								<input name="email"  type="email" placeholder="Email" onChange={ this.handleChange } value={ email }/>
							</label>
						</article>

						<article>
							<label>
								<strong>Password</strong>
								<input name="password" type="password" placeholder="Password" onChange={ this.handleChange } value={ password }/>
							</label>
						</article>
						<div className="ButtonGroup ButtonGroup--center">
							<button type="submit" className="Button Button--color3" title="Accounts are in closed beta for the moment.">Register a new account</button>
						</div>
					</form>
				</section>
				<section className="Section Note">
					<p>Already have an account? <Link to="/login" className="Button Button--color1">Login</Link></p>
				</section>
			</div>
		)
  }
}

export default withNotification( Register );
