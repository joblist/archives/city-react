import React from 'react';

export default function IntroText(props) {
	return (
		<div className={ `IntroText ${props.center ? 'IntroText--center' : ''}`}>
			{props.children}
		</div>
	)
}
