import React from 'react';


const Search = (props) => {
    const { handleSearch, search, clearSearch } = props
    return (
        <>
            <nav className="Nav Nav--sticky Nav--companies">
                <label className="FormItem FormItem--h">
                    <input
                        type="search"
                        title="Search for a company"
                        placeholder="Search all companies, descriptions, #tags..."
                        onChange={(e) => handleSearch(e.target.value)}
                        value={search} />
                </label>
                <button className={`Button ${!search.length && 'Button--hidden'}`} onClick={clearSearch}>Clear</button>
            </nav>
        </>
    )
}

export default Search;


