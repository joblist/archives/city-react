import React from 'react';
import {
	Route,
	Switch,
	Redirect } from 'react-router';
import JobsMap from './JobsMap';
import CompaniesMap from './CompaniesMap';

export default function JobsRoute(props) {
	const { match } = props;

	return (
		<Switch>
			<Redirect exact from={`${match.url}`} to={`${match.url}/jobs`}/>
			<Route path={`${match.url}/jobs/`} component={ JobsMap }/>
			<Route path={`${match.url}/companies`} component={ CompaniesMap }/>
		</Switch>
	)
}
