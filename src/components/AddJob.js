import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import firebase from 'firebase/app';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet'

import withNotification from './withNotification';
import IntroText from './IntroText';
import { postJob } from '../actions/jobs';

class AddJob extends Component {
  constructor() {
		super();
		this.state = {
			// page specific
			centerLat: null,
			centerLong: null,
			defaultLat: 52.49221,
			defaultLong: 13.43479,
			defaultZoom: 12,

			// new job model properties
	    title: '',
			body: '',
			url: '',
			jobLat: null,
			jobLong: null
		}
  }

  handleSubmit = (e) => {
		e.preventDefault();
		const { addNotification, history } = this.props;
		const { body, url, centerLat, centerLong } = this.state;
		let { title } = this.state;

		const latitude = centerLat
		const longitude = centerLong

		// remove unecessary white spaces
		title = title.trim()

		let position
		if (latitude && longitude) {
			position = new firebase.firestore.GeoPoint(latitude, longitude)
		}

		const job = {
			title,
			body,
			url,
			position: position || null
		}

		postJob(job).then(newJob => {
			addNotification(`Success, your job has been created!`);
			history.push(`/jobs/${newJob.id}`);
		}).catch(error => {
			console.log('AddJob submit error:', error);
		})
  }

  handleChange = (e) => {
		// if it is a URL fetch and set title
		this.setState({
	    [e.target.name] : e.target.value
		});
  }

	onViewportChanged = (viewport) => {
    this.setState({
			centerLat: viewport.center[0],
			centerLong: viewport.center[1]
		})
  }

	setMapPosition = () => {
		this.setState({
			jobLat: this.state.centerLat,
			jobLong: this.state.centerLong
		})
	}

	removeMapPosition = () => {
		this.setState({
			jobLat: null,
			jobLong: null
		})
	}


  render() {
		const {
			body,
			title,
			url,
			defaultLong,
			defaultLat,
			jobLong,
			jobLat
		} = this.state
		const position = [defaultLat, defaultLong]
		const jobHasPosition = jobLat && jobLong
		const markerPosition = [jobLat, jobLong]

		return (
			<div className="Page">
				<IntroText center>
					<h2>New job</h2>
					<p>
						<strong>Present the job</strong> and its requirements to candidates.
					</p>
				</IntroText>
				<article className="Card Card--job Card--add">
					<form onSubmit={ this.handleSubmit }>
						<article>
							<label>
								<strong>Title</strong>
								<input name="title"  type="text" placeholder="Job's title" onChange={ this.handleChange } value={ title }/>
								<p>A clear and short title for this job offer. This is the first thing candidates will read. You could also add the name of your company.</p>
							</label>
						</article>

						<article>
							<label>
								<strong>URL <small>(optional)</small></strong>
								<input name="url" type="url" placeholder="https://example.com/jobs/gardener" onChange={ this.handleChange } value={ url }/>
								<p>Add a link to the most detailed version of this job description. It could also be the link to a social media article, or an email address.</p>
							</label>
						</article>

						<article>
							<label>
								<strong>Geographic position <small>(optional)</small></strong>
								<Map
									className="Map Map--add"
									center={position}
									zoom={this.state.defaultZoom}
									onViewportChanged={this.onViewportChanged}>
									<TileLayer
									attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
									url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
									/>
									{ jobHasPosition ? (
											<Marker position={markerPosition}>
												<Popup>
													Job position
												</Popup>
											</Marker>
									) : (<></>)}</Map>
							</label>
							<nav className="Nav Nav--horizontal Nav--right">
								{ jobHasPosition ? (
										<button className="Button Button--color1 Nav-item" type="button" onClick={this.removeMapPosition}>Remove position</button>
								) : (<></>)}
								<button className="Button Button--color1 Nav-item" type="button" onClick={this.setMapPosition}>Set job position</button>
							</nav>
						</article>

						<article>
							<label>
								<strong>Description <small>(optional)</small></strong>
								<details>
									<summary>Info</summary>
									<p>
										Write everything a candidate needs to know to see if they match your needs.<br/>
										You can describe the position, how to apply, what skills your are looking for.<br/>
										You can use #hashtags to help user find your job.
									</p>
								</details>
								<textarea name="body" type="text" placeholder="Everything candidates should know about their future job." onChange={ this.handleChange } value={ body }/>
							</label>
						</article>

						<nav className="Nav Nav--horizontal Nav--right">
							<Link to="/jobs" className="Nav-item Button Button--color2 Button--back mr-1">Cancel</Link>
							<button className="Button Button--color3" type="submit">Create new job</button>
						</nav>
					</form>
				</article>
				<section>
					<details>
						<summary>What happens when creating a new job?</summary>
						<ul>
							<li>You <strong>can edit</strong> this job later</li>
							<li>A created job is <strong>always publicly accessible</strong> by all visitors to the site</li>
							<li>A created job will have to be <strong>published</strong> to be accessible from the homepage</li>
							<li>You are <strong>responsible for the accuracy of the information</strong> you provide</li>
						</ul>
					</details>
				</section>
			</div>
		)
  }
}

export default withNotification(AddJob);
