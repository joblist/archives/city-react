import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import withJob from '../withJob';
import withNotification from './withNotification';
import JobCard from './JobCard';
import IntroText from './IntroText';
import Checkout from './Checkout';
import { publishJob } from '../actions/jobs';

class PublishJob extends Component {

	constructor(){
		super()
		this.state = {
			isPublishing: false
		}
	}

	handlePublish({id}, token, coupon='beta') {
		if (!id) return false
		if (!token && !coupon) return false

		const { addNotification } = this.props;

		this.setState({
			isPublishing: true
		})

		return publishJob(id, token, coupon)
			.then(test => {
				const { history } = this.props;
				addNotification('Sucess! The job will be published in an instant.')
				history.push(`/jobs/${id}`);
			}).catch(error => {
				addNotification('There has been an error during the job publication. Contact an administrator.')
				this.state = {
					isPublishing: false
				}
			})
	}

	render() {
		if (!this.props.data || !this.props.data.id) return false
		const {data} = this.props
		const {isPublishing} = this.state
		const coupon = 'beta'

		return (
			<div>
				<IntroText center={true}>
					Publishing a job makes it <strong>available on the jobs homepage</strong>, to everybody visiting the site.
				</IntroText>
				<section className="Section">
				<JobCard data={data} mini={true}/>
				</section>

			{data.isPublished ? (
				<IntroText center={true}>
					<p>
						This Job is published!
					</p>
				</IntroText>
			) : (
				<IntroText center={true}>
					<p className="dn">
						<Checkout amount={1400} description="Publish a job" onToken={(token) => { this.handlePublish(data, token)}}>
							Publish this job now
						</Checkout>
					</p>
					<p>
						<button
							className="Button Button--color4"
							title="Make this job discoverable on the homepage"
							onClick={() => { this.handlePublish(data)}}
							disabled={isPublishing}>
							Publish this job now
						</button>
					</p>
				</IntroText>
			)}

				<div>

			</div>

				<section>
				<details>
				<summary>
				Info
			  </summary>
				<ul>
				<li>you can unpublish this job anytime</li>
				<li>it is always possible to edit a published job</li>
			  <li>not-published jobs are freely accessible to visitors, and can be shared by their URL</li>
				<li>if you need but cannot afford the publication, <Link to="/info">get in touch!</Link></li>
			  <li>payments are processed by <a href="https://stripe.com/">Stripe</a>, we do not store any information</li>
				</ul>
				</details>
				</section>
		</div>
		)
	}
}

export default withNotification(withJob(PublishJob))
