import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ContextualToggle from './ContextualToggle'
import Option from './Option';
import {format, differenceInDays} from 'date-fns'


class CompanyCard extends Component {
	reportBrokenLink(linkID) {
		const reportURL = `https://docs.google.com/forms/d/e/1FAIpQLSfBpCBvw7ApDW4ZUni85zqZyzHhoJ0aimNAdLc4Jnr_Pxzk7A/viewform?usp=pp_url&entry.727514964=${linkID}&entry.730769846`
		window.open(reportURL, '_blank');
	}

	reportSuggestion(linkID) {
		const reportURL = `https://docs.google.com/forms/d/e/1FAIpQLSeCFXeT92LbiQApCcFHmiWgUxaesV25iXrPNQ2RlcXwpvlsLw/viewform?usp=sf_link&entry.673651500=${linkID}`
		window.open(reportURL, '_blank');
	}

	openInMaps(linkTitle) {
		// the last bit adds the the bicyle lanes layer
		const mapUrl = 'https://www.google.com/maps/search/'
					+ encodeURIComponent(`${linkTitle} berlin germany`)
					+ '/data=!5m1!1e3'
		window.open(mapUrl, '_blank');
	}

	companyIsApproved() {
		if (this.props.data && this.props.data.isApproved) {
			return 'Card--isApproved';
		} else {
			return 'Card--isNotApproved';
		}
	}

	companyIsNew() {
		const diff = differenceInDays(new Date(), this.props.data.createdAt.toDate())
		return diff < 31
	}
	companyIsNewClass(isNew) {
		return isNew ? 'Card--isNew' : ''
	}

	render() {

		if (!this.props.data) return false

		const {
			id,
			title,
			url,
			body,
			tags,
			createdAt } = this.props.data;

		const isNew = this.companyIsNew()

		const humanDate = format(createdAt.toDate(), 'yyyy MMMM dd')

		return (
			<article
				className={`Card Card--company ${this.companyIsApproved()} ${this.companyIsNewClass(isNew)}`}>

				{ this.companyIsNew() && <span className="Card-new" title={`New! Created on ${humanDate}`}></span>}


				<div className="dn">
					<aside className="Card-toggle">
						<ContextualToggle label={ title }>
							<Option action={ () => this.reportBrokenLink(this.props.id) }>Report broken link &rarr;</Option>
							<Option action={ () => this.reportSuggestion(this.props.id) }>Suggest improvements &rarr;</Option>
							<Option action={ () => this.openInMaps(this.props.title) }>Show in maps &rarr;</Option>
						</ContextualToggle>
					</aside>
				</div>

				<div className="Card-main">
					<div className="Card-header">
						<a href={ url } className="Card-link" target="_blank" rel="noopener noreferrer" title={`Explore job offers on ${title}`}>
							<h4 className="Card-title">{ title }</h4>
							<input readOnly className="Card-url Input Input--link" value={ url }/>
						</a>
					</div>
					{ body && (
						<p className="Card-body">{ body }</p>
					) }
			{ tags && tags.length ? (
				<div className="Card-tags">
					{tags.map(tag => <Link to={`/tags/companies/${tag}`} className="Card-tag">{tag}</Link>)}
				</div>
			) : ''}
				<Link
			className="Card-permalink"
			to={ `/companies/${id}` }
			title={`Added to the site on: ${humanDate}`}>Permalink</Link>
				</div>
			</article>
		)
	}
}

export default CompanyCard;
