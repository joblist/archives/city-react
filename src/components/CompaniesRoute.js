import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Companies from './Companies';
import CompaniesUnpublished from './CompaniesUnpublished';
import CompaniesTagsRoute from './CompaniesTagsRoute';
import CompanyRoute from './CompanyRoute';

export default class CompaniesRoute extends Component {
	render() {
		const { match } = this.props;

		return (
			<div className="Page">
				<Switch>
					<Route exact path={match.url} component={ Companies }/>
					<Route exact path={`${match.url}/unpublished`} component={ CompaniesUnpublished }/>
					<Route exact path={`${match.url}/tags`} component={ CompaniesTagsRoute }/>
					<Route path={`${match.url}/:id`} component={ CompanyRoute }/>
				</Switch>
			</div>
		)
	}
}
