import React from 'react';
import { getCurrentUser } from './actions/auth';
import firebase from 'firebase/app';

const withFirebase = (buildRef, serializer) => {
	return ExtendedComponent => class extends React.Component {
		constructor() {
			super();
			this.state = {};
		}

		componentDidMount() {
			this.connectUser();
			this.connectData();
		}
		componentDidUpdate(prevProps) {
			// reconnect the data if the url params changed
			// usefull for #tags pages, when the tag changes in URL
			const prevPropsParams = JSON.stringify(
				prevProps &&
				prevProps.match &&
				prevProps.match.params
				|| ''
			)
			const propsParams = JSON.stringify(
				this.props &&
				this.props.match &&
				this.props.match.params
				|| '')
			if (prevPropsParams !== propsParams) {
				this.connectData();
			}
		}

		// disconnectData() {
		// 	this.state.datebaseRef.off()
		// }

		connectUser() {
			firebase.auth().onAuthStateChanged(user => {
				if (user) {
					this.setState({
						user
					})
				}
				this.connectData()
			})
		}

		connectData() {
			// if there is a user set it here
			getCurrentUser().then(user => {
				// connect database to receive the data
				// we want the extended component to have access to
				user = this.state.user || user;
				const databaseRef = buildRef({
					user: user,
					...this.props
				})

				if (!databaseRef) return

				databaseRef.onSnapshot(snapshot => {
					let data = typeof snapshot.data === 'function' && snapshot.data()
					let docs = snapshot.docs || []
					if (!data) {
						data = docs.map(doc => {
							let mappedData = doc.data()
							mappedData.id = doc.id
							return mappedData
						})
					} else {
						data.id = snapshot.id
					}

					const id = this.props
									&& this.props.match
									&& this.props.match.params
									&& this.props.match.params.id
									|| ''

					const sData = serializer(data, )

					data ? (
						this.setState({
							databaseRef,
							data: sData
						})
					) : (
						this.setState({
							databaseRef,
							error: {
								title: '404 — The page you requested was not found'
							}
						})
					)
				});
			})
		}

		// componentDidUpdate() {
		// }

		render() {
			return <ExtendedComponent { ...this.props } data={ this.state.data } user={ this.state.user }/>
		}
	}
}

export default withFirebase;
