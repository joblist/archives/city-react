import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeCompany from './serializers/company';

function buildRef(props) {
	return firebase.firestore()
		.collection('links')
		.doc(props.match.params.id)
}

export default withFirebase(
	buildRef,
	serializeCompany
);
