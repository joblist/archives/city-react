import firebase, { database } from 'firebase/app';

export function postCompany(company) {
	const {
		title,
		url,
		body,
		position
	} = company

	const newModelRef = firebase.firestore().collection('links')

	return newModelRef.add({
		url,
		title,
		body,
		position,
		isApproved: false,
		createdAt: firebase.firestore.FieldValue.serverTimestamp()
	})
}
