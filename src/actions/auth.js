import { database, auth } from 'firebase';

/*
	 Auth
*/

function getCurrentUser() {
	return new Promise((resolve, reject) => resolve(auth().currentUser) );
}

function isAuthenticated() {
	return auth().currentUser;
}

function updateUserEmail(email) {
	return auth().currentUser.updateEmail(email);
}

function sendPasswordResetEmail(email) {
	return auth().sendPasswordResetEmail(email);
}

function registerWithEmail(email, password) {
	return auth().createUserWithEmailAndPassword(email, password)
						 .then(sendVerificationEmail);
}

function logoutUser() {
	return auth().signOut();
}

function loginWithEmail(email, password) {
	return auth().signInWithEmailAndPassword(email, password)
}

function sendVerificationEmail() {
	return getCurrentUser().then(user => user.sendEmailVerification())
}


/*
	 Database
 */

function getCurrentUserRef() {
	return getCurrentUser().then(user => {
		return database().ref('user')
			.orderByChild('user')
										 .equalTo(user.uid)
										 .limitToFirst(1)
										 .once('value');
	})
}
function getCurrentUserSettingRef() {
	return getCurrentUser().then(user => {
		return database().ref('userSettings')
										 .orderByChild('user')
										 .equalTo(user.uid)
										 .limitToFirst(1)
										 .once('value');
	})
}

function createUserSetting() {
	return getCurrentUser().then(user => {
		var newSettingsRef = database().ref('userSettings').push()
		return newSettingsRef.set({
			user: user.uid
		})
	})
}

export {
	sendVerificationEmail,
	getCurrentUserSettingRef,
	getCurrentUser,
	isAuthenticated,
	updateUserEmail,
	sendPasswordResetEmail,
	registerWithEmail,
	logoutUser,
	loginWithEmail
}
