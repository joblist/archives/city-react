import firebase, { database } from 'firebase/app';
import 'firebase/functions';
import 'firebase/firestore';
import { getCurrentUser } from './auth';

export async function postJob(job) {
	const user = await getCurrentUser();

	const {
		title,
		body,
		url,
		position
	} = job

	return firebase.firestore().collection('jobs').add({
		title,
		body,
		url,
		position,
		createdAt: firebase.firestore.FieldValue.serverTimestamp(),
		user: user.uid
	})
}

export async function editJob(job) {
	const {
		id,
		title,
		body,
		url,
		position
	} = job

	if (!id) return false
	return firebase.firestore()
		.collection('jobs').doc(id)
		.update({
			title,
			body,
			url,
			position
		})
}

export async function deleteJob(id) {
	if (!id) return false
	return firebase.firestore()
		.collection('jobs').doc(id)
		.delete()
}

export async function publishJob(id, token, coupon) {
	const publish = firebase.functions().httpsCallable('publishJob')
	return publish({ id, token, coupon })
}
export async function unpublishJob(id) {
	return firebase.firestore()
		.collection('jobs').doc(id)
		.update({
			publishedAt: 0
		})
}

export default function userOwnsJob(user, job) {
	if(!user || !job) return false
	return user.uid === job.user
}
