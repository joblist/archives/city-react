import { map } from 'lodash';

export default function serializeList(data, serializer) {
	return map(data, serializer)
}
