import {differenceInDays} from 'date-fns'

const computeDaysLeft = (model) => {
	if(!model.publishedAt) return false
	const today = new Date()
	const pubDay = new Date(model.publishedAt.toDate())

	const diff =  differenceInDays(
		today,
		pubDay
	)

	return 31 - diff
}

export default function serializeJob(data, id) {
	if(!data) {
		data = {}
	}

	// database model
	const model = {
		id: '',
		body: '',
		url: '',
		title: '',
		createdAt: 0,
		publishedAt: 0,
		user: '',
		position: {}
	}

	let serializedModel = Object.assign(model, data)
	serializedModel.daysLeft = computeDaysLeft(serializedModel)
	serializedModel.isPublished = serializedModel.daysLeft > 0
	return serializedModel
};
