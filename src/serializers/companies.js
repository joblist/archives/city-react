import serializeList from './list'
import serializeCompany from './company'

const serializeCompanies = data => {
	return serializeList(data, serializeCompany)
}

export default serializeCompanies
