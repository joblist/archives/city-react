import serializeList from './list'
import serializeJob from './job'

const serializeJobs = data => {
	return serializeList(data, serializeJob)
}

export default serializeJobs
