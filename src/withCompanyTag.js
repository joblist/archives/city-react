import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeJobs from './serializers/jobs';

function buildRef(props) {
	return firebase.firestore()
		.collection('links')
		.where('isApproved', '==', true)
		.where('tags', 'array-contains', props.match.params.tag)
		.orderBy('createdAt')
}

export default withFirebase(buildRef, serializeJobs);
