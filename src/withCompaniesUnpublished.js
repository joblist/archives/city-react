import firebase from 'firebase/app';
import withFirebase from './withFirebase';
import serializeCompanies from './serializers/companies';

const buildRef = () => {
	return firebase.firestore()
		.collection('links')
		.where('isApproved', '==', false)
		.orderBy('createdAt')
}

export default withFirebase(buildRef, serializeCompanies);
